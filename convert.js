#!/usr/bin/env node
const fs = require('fs').promises
const p = require('path')
const dJSON = require('dirty-json')
const XML = require('xmlbuilder2')
// const debug = require('debug')('convert')
// const table = require('text-table')
// const _ = require('lodash')
const debug = console.log

const VALID_ENDINGS = ['json', 'meta.txt']

if (!process.argv[3]) usage()
run(process.argv[2], process.argv[3])
  .then(() => console.log('all done'))
  .catch(err => {
    console.error(err.message)
  })

async function run (src, dest) {
  let deststat = await fs.stat(dest)
  if (!deststat.isDirectory()) throw new Error('Not a directory: ' + dest)
  let files = await fs.readdir(src)
  files = files.filter(file => matchEndings(file))
  console.log(`start: ${files.length} files`)
  const nfos = []
  let i = 0
  for (const file of files) {
    let prefix = `${++i}/${files.length}`
    let pad = ' '.repeat(prefix.length) + ' '
    debug(`${prefix}: read ${file}`)
    try {
      const nfo = await convertFile(p.join(src, file))
      const xml = await createNfo(nfo)
      const name = file.replace(/\.meta\.txt$/, '')
      const basename = name.split('.').slice(0, -1).join('.')
      const targetfile = basename + '.nfo'
      await fs.writeFile(p.join(dest, targetfile), xml)
      nfos.push(nfo)
      debug(`${pad} nfo written`)
    } catch (err) {
      debug(`${pad} err: err: ${file}`, err.message)
      debug(err)
    }
  }
}

async function createNfo (nfo) {
  const root = XML.create({ version: '1.0', encoding: 'UTF-8', standalone: 'yes' })
    .ele('episodedetails')
  for (let [key, value] of Object.entries(nfo)) {
    root.ele(key).txt(value)
  }
  const xml = root.end({ prettyPrint: true })
  return xml
}

async function convertFile (path) {
  const buf = await fs.readFile(path)
  let json
  try {
    json = JSON.parse(buf.toString())
  } catch (err) {
    try {
      json = dJSON.parse(buf.toString())
    } catch (err) {
      throw err
    }
  }
  return mapToNfo(path, json)
}

function mapToNfo (path, json) {
  const info = json.videoJsonPlayer
  let genres = []
  if (info.category) genres.push(info.category.name)
  if (info.subcategory) genres.push(info.subcategory.name)
  if (info.categories) {
    info.categories.forEach(c => genres.push(c.name))
  }
  genres = genres.map(genre => {
    return genre.replace('/', ' – ')
  })

  let title = info.VTI
  if (info.subtitle && info.subtitle !== title) title = title + ' – ' + info.subtitle
  let showtitle
  if (title && title !== info.VTI) showtitle = info.VTI

  const nfo = {
    title,
    showtitle: showtitle,
    tagline: info.V7T,
    plot: info.VDE,
    thumb: info.VTU.IUR,
    uniqueid: info.VID,
    genre: genres.join(' / '),
    aired: info.VRA
  }
  return nfo
}

function usage () {
  console.log('USAGE: convert.js <srcdir> <destdir>')
  process.exit(1)
}

function matchEndings (file) {
  for (const ending of VALID_ENDINGS) {
    if (file.endsWith(ending)) return true
  }
  return false
}
